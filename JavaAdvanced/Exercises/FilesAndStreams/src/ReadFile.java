import java.io.FileInputStream;
import java.io.IOException;

public class ReadFile {
    public static void main(String[] args) {
        String resourceFolder = "filePath";

        String inputPath = resourceFolder + "input.txt";

        try (FileInputStream fis = new FileInputStream(inputPath)) {
            int oneByte = fis.read();
            while (oneByte >= 0) {
                System.out.print(Integer.toBinaryString(oneByte) + " ");
                oneByte = fis.read();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
